#!/bin/python

import requests
import argparse
import json
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import fast_selenium
import random
import time

KEYS = [('pxyr1', 'L5pwssLbspLkJoNJSExF'), ('asd2151', '4G1asvN45KnWA7iSCppY'), ('asdf1288','WpNpV6Es3v36oq2BWt4y'), ('asdf1289','yzESJ6GqSsK7rnRzkkfr')]

def network_logs(session_id, user,password):
    url = "https://api.browserstack.com/automate/builds.json"
    req = requests.get(url, auth=(user, password))
    json_obj = json.loads(req.text)[0]
    build_id = json_obj['automation_build']['hashed_id']

    url = "https://api.browserstack.com/automate/builds/{}/sessions/{}/networklogs".format(build_id, session_id)
    print(url, user, password)
    time.sleep(10)

    req = requests.get(url, auth=(user, password))
    json_obj = json.loads(req.text)
    try:
        json_obj = json.loads(req.text)
        return json_obj
    except Exception as e:
        print("Not json", e)
        print('text', req.text)
        return {'error': req.text}

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("browsers_json", help="Browsers json file")
    parser.add_argument("html_file", help="HTML file to load")

    args = parser.parse_args()

    browsers = json.load(open(args.browsers_json))

    c = 0
    try:
        for desired_cap in browsers:
            c += 1
            if c < 539:
                print('skipping',c)
                continue
            user, password = random.choice(KEYS)
            print(user, c)
            desired_cap['browserstack.networkLogs'] = 'true'

            driver = webdriver.Remote(
                command_executor='http://{}:{}@hub.browserstack.com:80/wd/hub'.format(user, password),
                desired_capabilities=desired_cap)

            driver.get("http://gifted-neumann-f28988.bitballoon.com/")
            session_id = driver.session_id
            driver.quit()

            network_obj = network_logs(session_id, user, password)
            filename = 'res_' + session_id + '.json'
            data = network_obj
            data['bs_data'] = desired_cap
            json.dump(data, open(filename, 'w'))
    except Exception as e:
        print("Error: ", c, str(e))


if __name__ == "__main__":
    main()
